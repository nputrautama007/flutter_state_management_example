import 'package:flutter_provider_example/domain/entity/restaurant_list_entity.dart';
import 'package:flutter_provider_example/domain/respository/restaurant_repository.dart';
import 'package:flutter_provider_example/domain/use_case/restaurant_use_case.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'list_restaurant_use_case_test.mocks.dart';

@GenerateMocks([RestaurantRepository, ListRestaurantEntity])
void main() {
  late RestaurantRepository restaurantRepositoryMock;
  late ListRestaurantUseCase listRestaurantUseCase;
  late ListRestaurantEntity listRestaurantEntity;

  setUp(() {
    restaurantRepositoryMock = MockRestaurantRepository();
    listRestaurantEntity = MockListRestaurantEntity();
    listRestaurantUseCase = ListRestaurantUseCaseImpl(
      restaurantRepository: restaurantRepositoryMock,
    );
  });

  test(
      'use case call get list restaurant method'
      ' should return restaurant list entity', () async {
    when(restaurantRepositoryMock.getListRestaurant())
        .thenAnswer((_) async => listRestaurantEntity);

    final result = await listRestaurantUseCase.getListRestaurant();
    expect(result, listRestaurantEntity);
    verify(restaurantRepositoryMock.getListRestaurant());
    verifyNoMoreInteractions(restaurantRepositoryMock);
  });

  test(
      'use case call search restaurant method'
      'with restaurant name parameter should return restaurant list entity',
      () async {
    when(restaurantRepositoryMock.getListRestaurantByName('restaurant name'))
        .thenAnswer((_) async => listRestaurantEntity);

    final result = await restaurantRepositoryMock
        .getListRestaurantByName('restaurant name');
    expect(result, listRestaurantEntity);
    verify(restaurantRepositoryMock.getListRestaurantByName('restaurant name'));
    verifyNoMoreInteractions(restaurantRepositoryMock);
  });
}
