import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_provider_example/domain/entity/restaurant_list_entity.dart';
import 'package:flutter_provider_example/domain/use_case/restaurant_use_case.dart';
import 'package:flutter_provider_example/presentation/bloc/list_restaurant_bloc.dart';
import 'package:flutter_provider_example/presentation/bloc/list_restaurant_event.dart';
import 'package:flutter_provider_example/presentation/bloc/list_restaurant_state.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'list_restaurant_bloc_test.mocks.dart';

@GenerateMocks([ListRestaurantUseCase])
void main() {
  late ListRestaurantUseCase listRestaurantUseCaseMock;

  setUp(() {
    listRestaurantUseCaseMock = MockListRestaurantUseCase();
  });

  blocTest<ListRestaurantBloc, ListRestaurantState>(
    'get list restaurant bloc add event get list restaurant'
    'should return GetListRestaurantLoadingState, GetListRestaurantLoadedState ',
    build: () {
      ListRestaurantEntity response = ListRestaurantEntity(
        listRestaurantEntity: [
          RestaurantEntity(
            id: 'id',
            name: "name",
            rating: 0.0,
            pictureId: "pictureId",
            description: 'description',
            city: 'city',
          ),
        ],
      );
      when(listRestaurantUseCaseMock.getListRestaurant())
          .thenAnswer((_) async => response);
      return ListRestaurantBloc(
          getListRestaurantUseCase: listRestaurantUseCaseMock);
    },
    act: (bloc) => bloc.add(GetListRestaurant()),
    expect: () => [
      isA<ListRestaurantLoadingState>(),
      isA<ListRestaurantLoadedState>(),
    ],
  );

  blocTest<ListRestaurantBloc, ListRestaurantState>(
    'search restaurant  bloc add event search restaurant with restaurant name'
    ' should return SearchRestaurantLoadingState, SearchRestaurantLoadedState ',
    build: () {
      ListRestaurantEntity response = ListRestaurantEntity(
        listRestaurantEntity: [
          RestaurantEntity(
            id: 'id',
            name: "name",
            rating: 0.0,
            pictureId: "pictureId",
            description: 'description',
            city: 'city',
          ),
        ],
      );
      when(listRestaurantUseCaseMock.getListRestaurantByName("restaurant"))
          .thenAnswer((_) async => response);
      return ListRestaurantBloc(
          getListRestaurantUseCase: listRestaurantUseCaseMock);
    },
    act: (bloc) => bloc.add(SearchRestaurant(searchText: 'restaurant')),
    expect: () => [
      isA<ListRestaurantLoadingState>(),
      isA<ListRestaurantLoadedState>(),
    ],
  );
}
