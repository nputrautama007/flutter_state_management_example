import 'package:flutter/material.dart';
import 'package:flutter_provider_example/presentation/view/home_screen_with_bloc.dart';
import 'package:flutter_provider_example/presentation/view/home_screen_with_provider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import 'data/datasource/restaurant_service.dart';
import 'data/repository/restaurant_repository_impl.dart';
import 'domain/use_case/restaurant_use_case.dart';
import 'presentation/provider/list_restaurant_view_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(360, 640),
      builder: () => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        builder: (context, widget) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
            child: widget!,
          );
        },
        home: SelectStateManagementScreen(),
      ),
    );
  }
}

class SelectStateManagementScreen extends StatelessWidget {
  SelectStateManagementScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 1.sh,
      width: 1.sw,
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextButton(
            style: TextButton.styleFrom(
              primary: Colors.white,
              backgroundColor: Colors.lightBlue,
            ),
            onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => HomeScreenWithBloc(),
              ),
            ),
            child: Text("To Restaurant list with bloc"),
          ),
          Padding(
            padding: EdgeInsets.only(
              top: 16.0,
            ),
            child: TextButton(
              style: TextButton.styleFrom(
                primary: Colors.white,
                backgroundColor: Colors.lightBlue,
              ),
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ChangeNotifierProvider(
                      create: (_) => ListRestaurantViewModel(
                            listRestaurantUseCase: ListRestaurantUseCaseImpl(
                              restaurantRepository: RestaurantRepositoryImpl(
                                restaurantService: RestaurantServiceImpl(),
                              ),
                            ),
                          ),
                      child: HomeScreenWithProvider()),
                ),
              ),
              child: Text("To Restaurant list with provider"),
            ),
          ),
        ],
      ),
    );
  }
}
