class ListRestaurantEntity {
  List<RestaurantEntity>? listRestaurantEntity;

  ListRestaurantEntity({this.listRestaurantEntity});
}

class RestaurantEntity {
  RestaurantEntity({
    this.id,
    this.name,
    this.description,
    this.pictureId,
    this.city,
    this.rating,
    this.menus,
  });

  final String? id;
  final String? name;
  final String? description;
  final String? pictureId;
  final String? city;
  final double? rating;
  final MenusEntity? menus;
}

class MenusEntity {
  MenusEntity({
    this.foods,
    this.drinks,
  });

  final List<ItemEntity>? foods;
  final List<ItemEntity>? drinks;
}

class ItemEntity {
  ItemEntity({
    this.name,
  });

  final String? name;
}
