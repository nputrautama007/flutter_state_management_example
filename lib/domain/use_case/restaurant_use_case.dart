import 'package:flutter_provider_example/domain/entity/restaurant_list_entity.dart';
import 'package:flutter_provider_example/domain/respository/restaurant_repository.dart';

abstract class ListRestaurantUseCase {
  Future<ListRestaurantEntity> getListRestaurant();

  Future<ListRestaurantEntity> getListRestaurantByName(String restaurantName);
}

class ListRestaurantUseCaseImpl extends ListRestaurantUseCase {
  RestaurantRepository restaurantRepository;

  ListRestaurantUseCaseImpl({
    required this.restaurantRepository,
  });

  @override
  Future<ListRestaurantEntity> getListRestaurant() =>
      restaurantRepository.getListRestaurant();

  @override
  Future<ListRestaurantEntity> getListRestaurantByName(String restaurantName) =>
      restaurantRepository.getListRestaurantByName(
        restaurantName,
      );
}
