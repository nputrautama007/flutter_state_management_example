import 'package:flutter/material.dart';
import 'package:flutter_provider_example/domain/entity/restaurant_list_entity.dart';
import 'package:flutter_provider_example/presentation/view/detail_restaurant/detail_restaurant_screen.dart';

abstract class RestaurantListRouter {
  goToDetailListRestaurant(context, RestaurantEntity restaurantEntity);
}

class RestaurantListRouterImpl extends RestaurantListRouter {
  @override
  goToDetailListRestaurant(context, RestaurantEntity restaurantEntity) =>
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) =>
              DetailRestaurantScreen(restaurantEntity: restaurantEntity),
        ),
      );
}
