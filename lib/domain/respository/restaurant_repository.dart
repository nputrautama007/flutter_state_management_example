import 'package:flutter_provider_example/domain/entity/restaurant_list_entity.dart';

abstract class RestaurantRepository {
  Future<ListRestaurantEntity> getListRestaurant();
  Future<ListRestaurantEntity> getListRestaurantByName(String restaurantName);
}