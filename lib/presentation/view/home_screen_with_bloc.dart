import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_provider_example/data/datasource/restaurant_service.dart';
import 'package:flutter_provider_example/data/repository/restaurant_repository_impl.dart';
import 'package:flutter_provider_example/domain/use_case/restaurant_use_case.dart';
import 'package:flutter_provider_example/external/custom_colors.dart';
import 'package:flutter_provider_example/presentation/bloc/list_restaurant_bloc.dart';
import 'package:flutter_provider_example/presentation/bloc/list_restaurant_event.dart';
import 'package:flutter_provider_example/presentation/bloc/list_restaurant_state.dart';
import 'package:flutter_provider_example/presentation/widget/card/restaurant_card.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeScreenWithBloc extends StatefulWidget {
  HomeScreenWithBloc({Key? key}) : super(key: key);

  @override
  _HomeScreenWithBlocState createState() => _HomeScreenWithBlocState();
}

class _HomeScreenWithBlocState extends State<HomeScreenWithBloc> {
  bool isSearching = false;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ListRestaurantBloc(
        getListRestaurantUseCase: ListRestaurantUseCaseImpl(
          restaurantRepository: RestaurantRepositoryImpl(
            restaurantService: RestaurantServiceImpl(),
          ),
        ),
      )..add(
          GetListRestaurant(),
        ),
      child: Scaffold(
        backgroundColor: CustomColors.yellow,
        appBar: AppBar(
          backgroundColor: CustomColors.yellow,
          elevation: 0.0,
          centerTitle: false,
          automaticallyImplyLeading: false,
          title: isSearching
              ? Row(
                  children: [
                    InkWell(
                      child: Icon(
                        Icons.clear,
                      ),
                      onTap: openSearch,
                    ),
                    Expanded(
                      child: Container(
                        height: 40.w,
                        padding: EdgeInsets.all(8.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          color: CustomColors.white,
                        ),
                        child: BlocBuilder<ListRestaurantBloc,
                            ListRestaurantState>(
                          builder: (context, state) {
                            return TextField(
                              onChanged: (value) =>
                                  BlocProvider.of<ListRestaurantBloc>(context)
                                      .add(
                                SearchRestaurant(
                                  searchText: value.toString(),
                                ),
                              ),
                              decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.fromLTRB(8.w, 5.w, 8.w, 10.w),
                                border: InputBorder.none,
                                hintText: "Cari restaurant",
                                hintStyle: TextStyle(
                                  color: CustomColors.grey,
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "Restaurant",
                      style:
                          TextStyle(color: CustomColors.white, fontSize: 20.sp),
                    ),
                    Text(
                      "Recommendation restaurant for you!",
                      style:
                          TextStyle(color: CustomColors.white, fontSize: 12.sp),
                    )
                  ],
                ),
          actions: [
            isSearching
                ? Container()
                : IconButton(
                    onPressed: openSearch,
                    icon: Icon(
                      Icons.search,
                      color: CustomColors.white,
                    ),
                  ),
          ],
        ),
        body: BlocBuilder<ListRestaurantBloc, ListRestaurantState>(
          builder: (context, state) {
            if (state is ListRestaurantLoadedState) {
              return SingleChildScrollView(
                physics: NeverScrollableScrollPhysics(),
                child: Container(
                  height: 1.sh,
                  margin: EdgeInsets.only(top: 16.w),
                  padding: EdgeInsets.all(16.w),
                  decoration: BoxDecoration(
                    color: CustomColors.lightYellow,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40.0),
                      topRight: Radius.circular(40.0),
                    ),
                  ),
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: state.listRestaurant!.listRestaurantEntity!.length,
                    itemBuilder: (context, index) {
                      return RestaurantCard(
                        restaurantEntity: state.listRestaurant!.listRestaurantEntity![index],
                      );
                    },
                  ),
                ),
              );
            } else {
              return Container(
                padding: EdgeInsets.all(16.w),
                decoration: BoxDecoration(
                  color: CustomColors.lightYellow,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40.0),
                    topRight: Radius.circular(40.0),
                  ),
                ),
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
          },
        ),
      ),
    );
  }

  openSearch() {
    setState(() {
      isSearching = !isSearching;
    });
  }
}
