import 'package:flutter/material.dart';
import 'package:flutter_provider_example/domain/entity/restaurant_list_entity.dart';
import 'package:flutter_provider_example/external/custom_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DrinksScreen extends StatelessWidget {
  final List<ItemEntity>? drinks;

  DrinksScreen({
    this.drinks,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: drinks!.length,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return Padding(
          padding: EdgeInsets.fromLTRB(16.w, 0.w, 16.w, 16.w),
          child: Text(
            drinks![index].name!,
            style: TextStyle(fontSize: 20.sp, color: CustomColors.darkGrey),
          ),
        );
      },
    );
  }
}
