import 'package:flutter/material.dart';
import 'package:flutter_provider_example/external/custom_colors.dart';
import 'package:flutter_provider_example/presentation/provider/list_restaurant_provider_state.dart';
import 'package:flutter_provider_example/presentation/provider/list_restaurant_view_model.dart';
import 'package:flutter_provider_example/presentation/widget/card/restaurant_card.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class HomeScreenWithProvider extends StatefulWidget {
  @override
  _HomeScreenWithProviderState createState() => _HomeScreenWithProviderState();
}

class _HomeScreenWithProviderState extends State<HomeScreenWithProvider> {
  bool isSearching = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.yellow,
      appBar: AppBar(
        backgroundColor: CustomColors.yellow,
        elevation: 0.0,
        centerTitle: false,
        automaticallyImplyLeading: false,
        title: isSearching
            ? Row(
                children: [
                  InkWell(
                    child: Icon(
                      Icons.clear,
                    ),
                    onTap: openSearch,
                  ),
                  Expanded(
                    child: Container(
                      height: 40.w,
                      padding: EdgeInsets.all(8.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        color: CustomColors.white,
                      ),
                      child: TextField(
                        onChanged: (value) =>
                            Provider.of<ListRestaurantViewModel>(context,
                                    listen: false)
                                .searchRestaurant(
                          value,
                        ),
                        decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.fromLTRB(8.w, 5.w, 8.w, 10.w),
                          border: InputBorder.none,
                          hintText: "Cari restaurant",
                          hintStyle: TextStyle(
                            color: CustomColors.grey,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Restaurant",
                    style:
                        TextStyle(color: CustomColors.white, fontSize: 20.sp),
                  ),
                  Text(
                    "Recommendation restaurant for you!",
                    style:
                        TextStyle(color: CustomColors.white, fontSize: 12.sp),
                  )
                ],
              ),
        actions: [
          isSearching
              ? Container()
              : IconButton(
                  onPressed: openSearch,
                  icon: Icon(
                    Icons.search,
                    color: CustomColors.white,
                  ),
                ),
        ],
      ),
      body: Consumer<ListRestaurantViewModel>(
        builder:
            (context, ListRestaurantViewModel listRestaurantViewModel, widget) {
          if (listRestaurantViewModel.listRestaurantProviderState ==
              ListRestaurantProviderState.Loaded) {
            return SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: Container(
                height: 1.sh,
                margin: EdgeInsets.only(top: 16.w),
                padding: EdgeInsets.all(16.w),
                decoration: BoxDecoration(
                  color: CustomColors.lightYellow,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40.0),
                    topRight: Radius.circular(40.0),
                  ),
                ),
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: listRestaurantViewModel
                      .listRestaurant!.listRestaurantEntity!.length,
                  itemBuilder: (context, index) {
                    return RestaurantCard(
                      restaurantEntity: listRestaurantViewModel
                          .listRestaurant!.listRestaurantEntity![index],
                    );
                  },
                ),
              ),
            );
          } else {
            return Container(
              padding: EdgeInsets.all(16.w),
              decoration: BoxDecoration(
                color: CustomColors.lightYellow,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40.0),
                  topRight: Radius.circular(40.0),
                ),
              ),
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        },
      ),
    );
  }

  openSearch() {
    setState(() {
      isSearching = !isSearching;
    });
  }
}
