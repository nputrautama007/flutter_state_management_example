import 'package:flutter/material.dart';
import 'package:flutter_provider_example/domain/entity/restaurant_list_entity.dart';
import 'package:flutter_provider_example/domain/use_case/restaurant_use_case.dart';

import 'list_restaurant_provider_state.dart';

class ListRestaurantViewModel with ChangeNotifier {
  final ListRestaurantUseCase listRestaurantUseCase;
  ListRestaurantProviderState listRestaurantProviderState =
      ListRestaurantProviderState.Initial;
  ListRestaurantEntity? listRestaurant;

  ListRestaurantViewModel({
    required this.listRestaurantUseCase,
  }) {
    getListRestaurant();
  }

  getListRestaurant() async {
    listRestaurantProviderState = ListRestaurantProviderState.Loading;
    listRestaurant = await listRestaurantUseCase.getListRestaurant();
    listRestaurantProviderState = ListRestaurantProviderState.Loaded;
    notifyListeners();
  }

  searchRestaurant(String restaurantName) async {
    listRestaurantProviderState = ListRestaurantProviderState.Loading;
    listRestaurant =
        await listRestaurantUseCase.getListRestaurantByName(restaurantName);
    listRestaurantProviderState = ListRestaurantProviderState.Loaded;
    notifyListeners();
  }
}
