import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_provider_example/domain/use_case/restaurant_use_case.dart';

import 'list_restaurant_event.dart';
import 'list_restaurant_state.dart';

class ListRestaurantBloc
    extends Bloc<ListRestaurantEvent, ListRestaurantState> {
  ListRestaurantUseCase getListRestaurantUseCase;

  ListRestaurantBloc({required this.getListRestaurantUseCase})
      : super(ListRestaurantInitialState());

  @override
  Stream<ListRestaurantState> mapEventToState(
      ListRestaurantEvent event) async* {
    if (event is GetListRestaurant) {
      yield ListRestaurantLoadingState();
      var listRestaurant = await getListRestaurantUseCase.getListRestaurant();
      yield ListRestaurantLoadedState(
        listRestaurant: listRestaurant,
      );
    } else if (event is SearchRestaurant) {
      yield ListRestaurantLoadingState();
      var _listRestaurant = await getListRestaurantUseCase
          .getListRestaurantByName(event.searchText);
      yield ListRestaurantLoadedState(listRestaurant: _listRestaurant);
    }
  }
}
