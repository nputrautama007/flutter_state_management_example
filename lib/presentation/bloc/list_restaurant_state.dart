import 'package:equatable/equatable.dart';
import 'package:flutter_provider_example/domain/entity/restaurant_list_entity.dart';

abstract class ListRestaurantState extends Equatable {
  const ListRestaurantState();
}

class ListRestaurantInitialState extends ListRestaurantState {
  @override
  List<Object> get props => [];
}

class ListRestaurantLoadingState extends ListRestaurantState {
  @override
  List<Object> get props => [];
}

class ListRestaurantLoadedState extends ListRestaurantState {
  final ListRestaurantEntity? listRestaurant;

  ListRestaurantLoadedState({
    this.listRestaurant,
  });

  @override
  List<Object> get props => [
    listRestaurant as Object,
  ];
}