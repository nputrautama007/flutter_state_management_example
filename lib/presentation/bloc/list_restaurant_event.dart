import 'package:equatable/equatable.dart';

abstract class ListRestaurantEvent extends Equatable {
  const ListRestaurantEvent();
}

class GetListRestaurant extends ListRestaurantEvent {
  @override
  List<Object> get props => [];
}

class SearchRestaurant extends ListRestaurantEvent {
  final String searchText;

  SearchRestaurant({
    this.searchText = "",
  });

  @override
  List<Object> get props => [
        searchText,
      ];
}
