class RestaurantListModel {
  RestaurantListModel({
    this.restaurants,
  });

  final List<Restaurant>? restaurants;

  factory RestaurantListModel.fromJson(Map<String, dynamic> json) =>
      RestaurantListModel(
        restaurants: json["restaurants"] == null
            ? null
            : List<Restaurant>.from(
                json["restaurants"].map((x) => Restaurant.fromJson(x))),
      );
}

class Restaurant {
  Restaurant({
    this.id,
    this.name,
    this.description,
    this.pictureId,
    this.city,
    this.rating,
    this.menus,
  });

  final String? id;
  final String? name;
  final String? description;
  final String? pictureId;
  final String? city;
  final double? rating;
  final Menus? menus;

  factory Restaurant.fromJson(Map<String, dynamic> json) => Restaurant(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        description: json["description"] == null ? null : json["description"],
        pictureId: json["pictureId"] == null ? null : json["pictureId"],
        city: json["city"] == null ? null : json["city"],
        rating: json["rating"] == null ? null : json["rating"].toDouble(),
        menus: json["menus"] == null ? null : Menus.fromJson(json["menus"]),
      );
}

class Menus {
  Menus({
    this.foods,
    this.drinks,
  });

  final List<Drink>? foods;
  final List<Drink>? drinks;

  factory Menus.fromJson(Map<String, dynamic> json) => Menus(
        foods: json["foods"] == null
            ? null
            : List<Drink>.from(json["foods"].map((x) => Drink.fromJson(x))),
        drinks: json["drinks"] == null
            ? null
            : List<Drink>.from(json["drinks"].map((x) => Drink.fromJson(x))),
      );
}

class Drink {
  Drink({
    this.name,
  });

  final String? name;

  factory Drink.fromJson(Map<String, dynamic> json) => Drink(
        name: json["name"] == null ? null : json["name"],
      );
}
