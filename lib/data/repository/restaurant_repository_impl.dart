import 'package:flutter_provider_example/data/datasource/restaurant_service.dart';
import 'package:flutter_provider_example/domain/entity/restaurant_list_entity.dart';
import 'package:flutter_provider_example/domain/respository/restaurant_repository.dart';

class RestaurantRepositoryImpl extends RestaurantRepository {
  RestaurantService restaurantService;

  RestaurantRepositoryImpl({
    required this.restaurantService,
  });

  @override
  Future<ListRestaurantEntity> getListRestaurant() async {
    List<RestaurantEntity> listRestaurant = [];
    var _restaurantData = await restaurantService.getRestaurantList();
    _restaurantData.restaurants!.forEach((restaurant) {
      List<ItemEntity> _foodList = [];
      restaurant.menus!.foods!.forEach((food) {
        var foodEntity = ItemEntity(name: food.name);
        _foodList.add(foodEntity);
      });

      List<ItemEntity> _drinkList = [];
      restaurant.menus!.drinks!.forEach((drink) {
        var drinkEntity = ItemEntity(name: drink.name);
        _drinkList.add(drinkEntity);
      });

      var _restaurantEntity = RestaurantEntity(
          id: restaurant.id,
          name: restaurant.name,
          description: restaurant.description,
          pictureId: restaurant.pictureId,
          city: restaurant.city,
          rating: restaurant.rating,
          menus: MenusEntity(
            foods: _foodList,
            drinks: _drinkList,
          ));
      listRestaurant.add(_restaurantEntity);
    });

    var restaurantListEntity = ListRestaurantEntity(
      listRestaurantEntity: listRestaurant,
    );

    return restaurantListEntity;
  }

  @override
  Future<ListRestaurantEntity> getListRestaurantByName(
      String restaurantName) async {
    List<RestaurantEntity> listRestaurant = [];
    List<RestaurantEntity> filterListRestaurant = [];
    var restaurantData = await restaurantService.getRestaurantList();
    restaurantData.restaurants?.forEach((restaurant) {
      List<ItemEntity> foodList = [];
      restaurant.menus!.foods!.forEach((food) {
        var foodEntity = ItemEntity(name: food.name);
        foodList.add(foodEntity);
      });

      List<ItemEntity> drinkList = [];
      restaurant.menus!.drinks!.forEach((drink) {
        var drinkEntity = ItemEntity(name: drink.name);
        drinkList.add(drinkEntity);
      });

      var restaurantEntity = RestaurantEntity(
          id: restaurant.id,
          name: restaurant.name,
          description: restaurant.description,
          pictureId: restaurant.pictureId,
          city: restaurant.city,
          rating: restaurant.rating,
          menus: MenusEntity(
            foods: foodList,
            drinks: drinkList,
          ));
      listRestaurant.add(restaurantEntity);
    });
    filterListRestaurant = listRestaurant
        .where((food) =>
            food.name!.toLowerCase().contains(restaurantName.toLowerCase()))
        .toList();

    var restaurantListEntity = ListRestaurantEntity(
      listRestaurantEntity: filterListRestaurant,
    );
    return restaurantListEntity;
  }
}
