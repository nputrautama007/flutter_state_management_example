import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter_provider_example/data/model/restaurant_list_model.dart';

abstract class RestaurantService {
  Future<RestaurantListModel> getRestaurantList();

  Future<RestaurantListModel> getRestaurantListByName();
}

class RestaurantServiceImpl extends RestaurantService {
  @override
  Future<RestaurantListModel> getRestaurantList() async {
    return await rootBundle
        .loadString('asset/list_restaurant.json')
        .then(
          (localRestaurant) => RestaurantListModel.fromJson(
            jsonDecode(
              localRestaurant,
            ),
          ),
        );
  }

  @override
  Future<RestaurantListModel> getRestaurantListByName() async {
    return await rootBundle
        .loadString('asset/list_restaurant.json')
        .then(
          (localRestaurant) => RestaurantListModel.fromJson(
            jsonDecode(
              localRestaurant,
            ),
          ),
        );
  }
}
